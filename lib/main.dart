import 'package:flutter/material.dart';

void main() {
  runApp(new app());
}

class app extends StatefulWidget {
  @override
  _appState createState() => _appState();
}

class _appState extends State<app> {

  String ttext = '';

  @override
  void initState() {
    // TODO: implement initState
    ttext = 'Click me!';
    super.initState();
  }

  void method1() {
    setState(() {
      ttext = 'Text Changed!';
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Stateful Widget',
      home: new Scaffold(
        body: new Center(
          child: new RaisedButton(onPressed:(){method1();}, child: new Text(ttext))
        )
      )
    );
  }
}
